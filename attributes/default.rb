default[:memcached][:release] = "http://memcached.googlecode.com/files/memcached-1.4.15.tar.gz"
default[:memcached][:libevent][:release] = "https://github.com/downloads/libevent/libevent/libevent-2.0.20-stable.tar.gz"

libevent  = "#{File.basename node.memcached.libevent.release}".gsub /.tar.gz$/, ""
memcached = "#{File.basename node.memcached.release}".gsub /.tar.gz$/, ""

default[:memcached][:prefix] = "#{ENV['HOME']}/.memcached"
default[:memcached][:home] = "#{default.memcached.prefix}/#{memcached}"
default[:memcached][:libevent][:prefix] = default.memcached.prefix
default[:memcached][:libevent][:home] = "#{default.memcached.libevent.prefix}/#{libevent}"
default[:memcached][:bin_dir] = "#{default.memcached.prefix}/bin"
default[:memcached][:service_dir] = default.memcached.prefix
default[:memcached][:service] = "#{default.memcached.service_dir}/memcached"
