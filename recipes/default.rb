directory node.memcached.service_dir

template node.memcached.service do
  mode 0755
end

install_tarball node.memcached.release

install_tarball node.memcached.libevent.release

home = node.memcached.home

script "libevent configure" do
  interpreter "bash"
  code "cd #{node.memcached.libevent.home}; ./configure --prefix=#{home}"
  not_if {File.exists? "#{node.memcached.libevent.home}/Makefile"}
end

script "libevent make install" do
  interpreter "bash"
  code "cd #{node.memcached.libevent.home}; make && make install"
end

script "memcached configure" do
  interpreter "bash"
  code "cd #{node.memcached.home}; ./configure --prefix=#{home} --with-libevent=#{home}/lib --mandir=#{home}/share/man"
  not_if {File.exists? "#{node.memcached.home}/Makefile"}
end

script "memcached make install" do
  interpreter "bash"
  code "cd #{node.memcached.home}; make && make install"
  not_if {File.exists? "#{node.memcached.home}/memcached"}
end
